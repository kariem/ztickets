/**
 * Copyright 2010 The PlayN Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package playn.sample.cute.core;

import static playn.core.PlayN.*;

import playn.core.Game;
import playn.core.Json;
import playn.core.Keyboard;
import playn.core.Pointer;
import playn.core.Surface;
import playn.core.SurfaceLayer;
import playn.core.util.Callback;

public class ZombieTicketsGame implements Game, Keyboard.Listener {

  private SurfaceLayer gameLayer;

  private CuteWorld world;
  private CuteObject hero;

  private boolean controlLeft, controlRight, controlUp, controlDown;
  private boolean controlJump;
  private float touchVectorX, touchVectorY;

  @Override
  public void init() {
    graphics().setSize(800, 600);

    gameLayer = graphics().createSurfaceLayer(graphics().width(), graphics().height());
    graphics().rootLayer().add(gameLayer);

    keyboard().setListener(this);
    pointer().setListener(new Pointer.Listener() {
      @Override
      public void onPointerEnd(Pointer.Event event) {
        touchVectorX = touchVectorY = 0;
      }
      @Override
      public void onPointerDrag(Pointer.Event event) {
        touchMove(event.x(), event.y());
      }
      @Override
      public void onPointerStart(Pointer.Event event) {
        touchMove(event.x(), event.y());
      }
    });

    // TODO(jgw): Until net is filled in everywhere, create a simple grass world.

    world = new CuteWorld(16, 16);

    // Grass.
    for (int y = 0; y < 16; ++y) {
      for (int x = 0; x < 16; ++x) {
        world.addTile(x, y, 2);
      }
    }

    initStuff();
  }

  private void initStuff() {
    hero = new CuteObject(assetManager().getImage("images/character_cat_girl.png"));
    hero.setPos(2, 2, 1);
    hero.r = 0.3;
    world.addObject(hero);
  }

  @Override
  public void onKeyDown(Keyboard.Event event) {
    switch (event.keyCode()) {
      case Keyboard.KEY_SPACE:
        controlJump = true;
        break;

      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
      case '8':
        addTile((int) hero.x, (int) hero.y, event.keyCode() - '1');
        break;

      case 'W':
        addTile((int) hero.x, (int) hero.y, 8);
        break;
      case 'D':
        addTile((int) hero.x, (int) hero.y, 9);
        break;
      case 'S':
        addTile((int) hero.x, (int) hero.y, 10);
        break;
      case 'A':
        addTile((int) hero.x, (int) hero.y, 11);
        break;

      case 'T':
        addTile((int) hero.x, (int) hero.y, 12);
        break;
      case 'Y':
        addTile((int) hero.x, (int) hero.y, 13);
        break;
      case 'H':
        addTile((int) hero.x, (int) hero.y, 14);
        break;
      case 'N':
        addTile((int) hero.x, (int) hero.y, 15);
        break;
      case 'B':
        addTile((int) hero.x, (int) hero.y, 16);
        break;
      case 'V':
        addTile((int) hero.x, (int) hero.y, 17);
        break;
      case 'F':
        addTile((int) hero.x, (int) hero.y, 18);
        break;
      case 'R':
        addTile((int) hero.x, (int) hero.y, 19);
        break;

      case Keyboard.KEY_ESC:
        removeTopTile((int) hero.x, (int) hero.y);
        break;

      case Keyboard.KEY_LEFT:
        controlLeft = true;
        break;
      case Keyboard.KEY_UP:
        controlUp = true;
        break;
      case Keyboard.KEY_RIGHT:
        controlRight = true;
        break;
      case Keyboard.KEY_DOWN:
        controlDown = true;
        break;
    }
  }

  @Override
  public void onKeyUp(Keyboard.Event event) {
    switch (event.keyCode()) {
      case Keyboard.KEY_LEFT:
        controlLeft = false;
        break;
      case Keyboard.KEY_UP:
        controlUp = false;
        break;
      case Keyboard.KEY_RIGHT:
        controlRight = false;
        break;
      case Keyboard.KEY_DOWN:
        controlDown = false;
        break;
    }
  }

  @Override
  public void update(float delta) {
    if (world == null) {
      return;
    }

    hero.setAcceleration(0, 0, 0);

    if (hero.isResting()) {
      // Keyboard control.
      if (controlLeft) {
        hero.ax = -1.0;
      }
      if (controlRight) {
        hero.ax = 1.0;
      }
      if (controlUp) {
        hero.ay = -1.0;
      }
      if (controlDown) {
        hero.ay = 1.0;
      }

      // Mouse Control.
      hero.ax += touchVectorX;
      hero.ay += touchVectorY;

      // Jump Control.
      if (controlJump) {
        hero.vz = 0.2;
        controlJump = false;
      }
    }

    world.updatePhysics(delta / 1000);
  }

  @Override
  public void paint(float alpha) {
    if (world == null) {
      return;
    }

    world.setViewOrigin(hero.x(alpha), hero.y(alpha), hero.z(alpha));

    Surface surface = gameLayer.surface();
    surface.clear();
    world.paint(surface, alpha);
  }

  private void touchMove(float x, float y) {
    float cx = graphics().screenWidth() / 2;
    float cy = graphics().screenHeight() / 2;

    touchVectorX = (x - cx) * 1.0f / cx;
    touchVectorY = (y - cy) * 1.0f / cy;
  }

  private void addTile(int x, int y, int type) {
    world.addTile(x, y, type);

    Json.Writer w = json().newWriter();
    w.object();
    w.key("op"); w.value("addTop");
    w.key("x"); w.value(x);
    w.key("y"); w.value(y);
    w.key("type"); w.value(type);
    w.endObject();

    post(w.write());
  }

  private void removeTopTile(int x, int y) {
    world.removeTopTile(x, y);

    Json.Writer w = json().newWriter();
    w.object();
    w.key("op"); w.value("removeTop");
    w.key("x"); w.value(x);
    w.key("y"); w.value(y);
    w.endObject();

    post(w.write());
  }

  private void post(String payload) {
    net().post("/rpc", payload, new Callback<String>() {
      @Override
      public void onSuccess(String response) {
        // Nada.
      }

      @Override
      public void onFailure(Throwable error) {
        // TODO
      }
    });
  }

  @Override
  public int updateRate() {
    return 33;
  }
}
