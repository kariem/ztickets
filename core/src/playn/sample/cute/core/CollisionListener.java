package playn.sample.cute.core;

import org.jbox2d.callbacks.*;
import org.jbox2d.collision.Manifold;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.Fixture;
import org.jbox2d.dynamics.contacts.*;

public class CollisionListener implements ContactListener {

  public void beginContact(Contact contact) {
    Fixture fixtureA = contact.getFixtureA();
    Fixture fixtureB = contact.getFixtureB();

    Object dataA = fixtureA.getUserData();
    Object dataB = fixtureB.getUserData();

    if (dataA instanceof Body || dataB instanceof Body) {
      //You loose
      System.out.println("You loose");
    } else if (dataA instanceof Zombie) {
      if (dataB instanceof Seat) {
        if (((Zombie) dataA).path.getSeat() == (Seat) dataB)
          ((Zombie) dataA).next();
      }
    } else if (dataA instanceof Seat) {
      if (dataB instanceof Zombie) {
        if (((Zombie) dataB).path.getSeat() == (Seat) dataA)
          ((Zombie) dataB).next();
      }
    }
  }

  public void endContact(Contact contact) {

  }

  public void postSolve(Contact contact, ContactImpulse impulse) {

  }

  public void preSolve(Contact contact, Manifold oldManifold) {

  }
}
