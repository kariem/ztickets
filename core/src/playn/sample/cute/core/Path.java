package playn.sample.cute.core;

import org.jbox2d.common.Vec2;

import java.util.ArrayList;

public class Path {
  public ArrayList<Seat> seats;
  public int idx = 0;
  public boolean forward;

  public Path() {
    seats = new ArrayList<Seat>();
    forward = true;
  }

  public void createSeat(int x, int y) {
    Seat s = new Seat(x, y);

    seats.add(s);
    idx++;
  }

  public void next() {
    if (forward) {
      idx--;
      if (idx < 0) {
        idx = 1;
        forward = false;
      }
    } else {
      idx++;
      if (idx >= seats.size()) {
        idx = seats.size() - 2;
        forward = true;
      }
    }
  }
  
  public Seat getSeat() {
     return seats.get(idx);
  }

  public void paint() {
    for (Seat s : seats) {
      s.paint();
    }
  }

  public Vec2 getSeatPosition() {
    return seats.get(idx).getVec2();
  }
}
