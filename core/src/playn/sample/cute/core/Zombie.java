package playn.sample.cute.core;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.MathUtils;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.*;

public class Zombie {

  private World m_world;
  
  public Body triangleBody;
  public Body boxBody;
  
  public Path path;
  
  public Vec2 direction = new Vec2(0.0f, 0.0f);
  
  public static final float SPEED = 2;

  public Zombie(World world, Path path) {
    this.m_world = world;
    this.path = path;
  }

  public void init() {
    float xLo = -5.0f, xHi = 5.0f;
    float yLo = 2.0f, yHi = 35.0f;

    Filter triangleFilter = new Filter();
    Filter boxFilter = new Filter();

    triangleFilter.groupIndex = 0x0002;
    triangleFilter.maskBits = 0x0002;

    boxFilter.groupIndex = 0x0001;
    boxFilter.maskBits = 0x0001;

    // A triangle
    Vec2 vertices[] = new Vec2[3];

    PolygonShape polygon = new PolygonShape();

    FixtureDef triangleShapeDef = new FixtureDef();
    triangleShapeDef.shape = polygon;
    triangleShapeDef.density = 1.0f;
    triangleShapeDef.isSensor = true;
    triangleShapeDef.filter = triangleFilter;

    BodyDef triangleBodyDef = new BodyDef();
    triangleBodyDef.type = BodyType.DYNAMIC;
    

    vertices[0].mulLocal(2.0f);
    vertices[1].mulLocal(2.0f);
    vertices[2].mulLocal(2.0f);
    polygon.set(vertices, 3);

    triangleBodyDef.position.set(MathUtils.randomFloat(xLo, xHi), MathUtils.randomFloat(yLo, yHi));

    triangleBody = m_world.createBody(triangleBodyDef);
    triangleBody.createFixture(triangleShapeDef);

    // Character Box
    polygon.setAsBox(1.0f, 0.5f);

    FixtureDef boxShapeDef = new FixtureDef();
    boxShapeDef.shape = polygon;
    boxShapeDef.density = 1.0f;
    boxShapeDef.filter = boxFilter;

    BodyDef boxBodyDef = new BodyDef();
    boxBodyDef.type = BodyType.DYNAMIC;

    polygon.setAsBox(1.0f, 1.0f);
    boxBodyDef.position.set(MathUtils.randomFloat(xLo, xHi), MathUtils.randomFloat(yLo, yHi));

    boxBody = m_world.createBody(boxBodyDef);
    boxBody.createFixture(boxShapeDef);
    
    triangleBody.setUserData(triangleBody);
    boxBody.setUserData(this);
  }
  
  public void next() {
   path.next();
   
   direction = boxBody.getPosition().negate().add(path.getSeatPosition()).abs().normalize().mul(SPEED);
  }
  
  public void update() {
   boxBody.applyForce(direction, body.getWorldCenter());
  }

  public void paint() {
    //current position: body.getPosition
    path.paint();
  }
}
