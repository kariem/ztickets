package playn.sample.cute.core;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.MathUtils;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.*;

public class Seat {

  private Vec2 position;
  private World m_world;
  
  private Body body;

  public Seat(float x, float y) {
    position = new Vec2(x, y);
  }

  public void init() {
    float xLo = -5.0f, xHi = 5.0f;
    float yLo = 2.0f, yHi = 35.0f;
    
    Filter boxFilter = new Filter();

    boxFilter.groupIndex = 0x0001;
    boxFilter.maskBits = 0x0001;

    // Seat Box
    PolygonShape polygon = new PolygonShape();
    
    polygon.setAsBox(1.0f, 0.5f);

    FixtureDef boxShapeDef = new FixtureDef();
    boxShapeDef.shape = polygon;
    boxShapeDef.density = 1.0f;
    boxShapeDef.filter = boxFilter;

    BodyDef boxBodyDef = new BodyDef();
    boxBodyDef.type = BodyType.STATIC;

    polygon.setAsBox(2.0f, 1.5f);
    boxBodyDef.position = position;

    body = m_world.createBody(boxBodyDef);
    body.createFixture(boxShapeDef);
    
    body.setUserData(this);
  }

  public Vec2 getVec2() {
    return position;
  }

  public void paint() {
      //paint at position
  }
}
