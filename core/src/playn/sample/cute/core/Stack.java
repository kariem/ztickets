package playn.sample.cute.core;

import playn.sample.cute.core.CuteObject;

import java.util.ArrayList;
import java.util.List;

public class Stack {
  public int[] tiles;
  public List<CuteObject> objects = new ArrayList<CuteObject>();

  public int height() {
    return tiles.length;
  }
}
