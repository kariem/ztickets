package playn.sample.cute.core;

import com.sun.org.apache.xerces.internal.impl.dtd.XMLDTDLoader;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.MathUtils;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.*;

public class Player {
  private PolygonShape polygon;
  private World m_world;
  
  private float xStart;
  private float yStart;
  
  private Body body;

  public Player(String spritesheet, float xStart, float yStart) {

   this.xStart = xStart;
   this.yStart = yStart;
  }

  public void init() {
    float xLo = -5.0f, xHi = 5.0f;
    float yLo = 2.0f, yHi = 35.0f;

    Filter boxFilter = new Filter();

    boxFilter.groupIndex = 0x0003;
    boxFilter.maskBits = 0x0003;

    // Character Box
    polygon.setAsBox(1.0f, 0.5f);

    FixtureDef boxShapeDef = new FixtureDef();
    boxShapeDef.shape = polygon;
    boxShapeDef.density = 1.0f;
    boxShapeDef.filter = boxFilter;

    BodyDef boxBodyDef = new BodyDef();
    boxBodyDef.type = BodyType.DYNAMIC;

    polygon.setAsBox(2.0f, 1.0f);
    boxBodyDef.position.set(xStart, yStart);

    body = m_world.createBody(boxBodyDef);
    body.createFixture(boxShapeDef);

    body.setUserData(this);
  }

   //could be called in the OnPress (not onDown, or onUp)
  public void move(int x, int y) {
    body.applyForce(new Vec2(x, y), body.getWorldCenter());
  }

  public void paint() {
      //body.getPosition
  }
}
